/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;


import co.edu.sena.censoweb.model.Predio;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface PredioBeanLocal {
    public void insert(Predio predio) throws Exception;
    public void update(Predio predio) throws Exception;
    public void delete(Predio predio) throws Exception;
    public Predio findById(Integer idPredio) throws Exception;
    public List<Predio> findAll() throws Exception;
}
