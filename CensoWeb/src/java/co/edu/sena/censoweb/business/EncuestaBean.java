/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuesta;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import co.edu.sena.censoweb.persistence.IEncuestaDAO;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class EncuestaBean implements EncuestaBeanLocal {
    
    @EJB
    private IEncuestaDAO iEncuestaDAO;
    
    public void validate(Encuesta encuesta) throws Exception{
        if(encuesta==null){
            throw new Exception("La encuesta es nula");
        }
        if(encuesta.getFecha()==null){
            throw new Exception("La fecha es obligatorio");
        }
        if(encuesta.getIdEncuestador()==null){
            throw new Exception("El id encuestador es obligatorio");
        }
        if(encuesta.getIdPredio()==null){
            throw new Exception("El id predio es obligatorio");
        }
        if(encuesta.getIdServicio()==null){
            throw new Exception("El id servicio es obligatorio");
        }
        if(encuesta.getIdSuscriptor()==null){
            throw new Exception("El id suscriptor es obligatorio");
        }
        
    }
    @Override
    public void insert(Encuesta encuesta) throws Exception {
        validate(encuesta);
        iEncuestaDAO.insert(encuesta);
    }

    @Override
    public void update(Encuesta encuesta) throws Exception {
        validate(encuesta);
        Encuesta oldEncuesta = iEncuestaDAO.findById(encuesta.getNumeroFormulario());
        if(oldEncuesta == null){
            throw new Exception("NO existe esa encuesta");
        }
        //merge
        oldEncuesta.setFecha(encuesta.getFecha());
        oldEncuesta.setIdEncuestador(encuesta.getIdEncuestador());
        oldEncuesta.setIdPredio(encuesta.getIdPredio());
        oldEncuesta.setIdServicio(encuesta.getIdServicio());
        oldEncuesta.setIdSuscriptor(encuesta.getIdSuscriptor());
        iEncuestaDAO.update(encuesta);  
    }

    @Override
    public void delete(Encuesta encuesta) throws Exception {
        validate(encuesta);
        Encuesta oldEncuesta = iEncuestaDAO.findById(encuesta.getNumeroFormulario());
        if(oldEncuesta == null){
            throw new Exception("NO existe esa encuesta");
        }
        iEncuestaDAO.delete(encuesta); 
    }

    @Override
    public Encuesta findById(Integer numero_formulario) throws Exception {
        if(numero_formulario==0){
            throw new Exception("El numero formulario es obligatorio");
        }
        return iEncuestaDAO.findById(numero_formulario);
    }

    @Override
    public List<Encuesta> findAll() throws Exception {
        return iEncuestaDAO.findAll();
    }    
}
