/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.UsoComercial;
import java.util.List;
import javax.ejb.Local;

/**
 * fecha:11/07/2022
 * @author Aprendiz
 * Objetivo: interfaz para el modelo uso comercial
 */
@Local
public interface IUsoComercialDAO {
    public void insert(UsoComercial usoComercial) throws Exception;
    public void update(UsoComercial usoComercial) throws Exception;
    public void delete(UsoComercial usoComercial) throws Exception;
    public UsoComercial findById(Integer idUso) throws Exception;
    public List<UsoComercial> findAll() throws Exception;
}
