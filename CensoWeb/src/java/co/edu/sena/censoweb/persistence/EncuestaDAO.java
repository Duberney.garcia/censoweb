/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Encuesta;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class EncuestaDAO implements IEncuestaDAO {
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Encuesta encuesta) throws Exception {
        try {
           entityManager.persist(encuesta);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Encuesta encuesta) throws Exception {
        try {
           entityManager.merge(encuesta);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Encuesta encuesta) throws Exception {
         try {
           entityManager.remove(encuesta);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public Encuesta findById(Integer numero_formulario ) throws Exception {
        
        try {
           return entityManager.find(Encuesta.class, numero_formulario);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<Encuesta> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Encuesta.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
