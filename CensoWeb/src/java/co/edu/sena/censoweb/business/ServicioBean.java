/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Servicio;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import co.edu.sena.censoweb.persistence.IServicioDAO;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ServicioBean implements ServicioBeanLocal{
    
    @EJB
    private IServicioDAO serviciosDAO;
    public void validate(Servicio servicios)throws Exception{
        if (servicios ==null) {
            throw new Exception("El uso comercial es nulo");
        }
        if (servicios.getAcueducto().isEmpty()) {
            throw new Exception("La acueducto es obligatorio");
        }
         if (servicios.getAlcantarillado().isEmpty()) {
            throw new Exception("el alcantarillado es obligatoria");
        }
         
    }

    @Override
    public void insert(Servicio servicios) throws Exception {
         validate(servicios);
        Servicio oldsServicios = serviciosDAO.findById(servicios.getIdServicio());
        if (oldsServicios!=null) {
            throw new Exception("ya existe un servicio con la mismo cedula");
        }
        
        serviciosDAO.insert(servicios);
    }

    @Override
    public void update(Servicio servicios) throws Exception {
         validate(servicios);
        Servicio oldsServicios = serviciosDAO.findById(servicios.getIdServicio());
        if (oldsServicios==null) {
            throw new Exception("ya existe un servicio con la mismo cedula");
        }
        //merge
        oldsServicios.setAcueducto(servicios.getAcueducto());
        oldsServicios.setAlcantarillado(servicios.getAlcantarillado());
        oldsServicios.setLectura(servicios.getLectura());
        oldsServicios.setMarcaMedidor(servicios.getMarcaMedidor());
        oldsServicios.setSerieMedidor(servicios.getSerieMedidor());
        serviciosDAO.update(servicios);
    }

    @Override
    public void delete(Servicio servicios) throws Exception {
        validate(servicios);
        Servicio oldsServicios = serviciosDAO.findById(servicios.getIdServicio());
        if (oldsServicios==null) {
            throw new Exception("ya existe un servicio con la mismo cedula");
        }
        
        serviciosDAO.delete(servicios);
    }

    @Override
    public Servicio findById(Integer id_servicios) throws Exception {
        if (id_servicios ==0) {
           throw new Exception("El id_servicios es obligatorio");
        }
        return serviciosDAO.findById(id_servicios);
    }

    @Override
    public List<Servicio> findAll() throws Exception {
        return serviciosDAO.findAll();
    }
}
