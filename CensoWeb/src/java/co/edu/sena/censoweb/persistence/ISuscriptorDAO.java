/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Suscriptor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ISuscriptorDAO {
    public void insert(Suscriptor suscriptor) throws Exception;
    public void update(Suscriptor suscriptor) throws Exception;
    public void delete(Suscriptor suscriptor) throws Exception;
    public Suscriptor findById(long documento) throws Exception;
    public List<Suscriptor> findAll() throws Exception;
}
