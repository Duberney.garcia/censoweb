/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Predio;
import co.edu.sena.censoweb.persistence.IPredioDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author usuario
 */
@Stateless
public class PredioBean implements PredioBeanLocal{
    @EJB
    private IPredioDAO predioDAO;
    
    public void validate(Predio predio)throws Exception{
        if (predio ==null) {
            throw new Exception("El uso comercial es nulo");
        }
        if (predio.getAbreviatura().isEmpty()) {
            throw new Exception("La abreeviatura es obligatorio");
        }
         if (predio.getPrimerNumero()==null) {
            throw new Exception("el primer pisoes obligatoria");
        }
         if (predio.getSegundoNumero()==null) {
            throw new Exception("El segundo numero es obligatorio");
        }
          if (predio.getTercerNumero()==null) {
            throw new Exception("El tercer numero es obligatorio");
            
        }
           if (predio.getBarrio().isEmpty()) {
            throw new Exception("El barrio es obligatorio");
        }
    }
    @Override
    public void insert(Predio predio) throws Exception {
        validate(predio);
        Predio oldPredio = predioDAO.findById(predio.getIdPredio());
        if (oldPredio!=null) {
            throw new Exception("ya existe un encuestador con la mismo cedula");
        }
        
        predioDAO.insert(predio);
    }

    @Override
    public void update(Predio predio) throws Exception {
        validate(predio);
        Predio oldPredio = predioDAO.findById(predio.getIdPredio());
        if (oldPredio==null) {
            throw new Exception("ya existe un encuestador con la mismo cedula");
        }
        //marge
        oldPredio.setAbreviatura(predio.getAbreviatura());
        oldPredio.setBarrio(predio.getBarrio());
        oldPredio.setComplemento(predio.getComplemento());
        oldPredio.setNumeroOcupantes(predio.getNumeroOcupantes());
        oldPredio.setIdUso(predio.getIdUso());
        oldPredio.setNumeroPisos(predio.getNumeroPisos());
        oldPredio.setPrimerNumero(predio.getPrimerNumero());
        oldPredio.setSegundoNumero(predio.getSegundoNumero());
        oldPredio.setTercerNumero(predio.getTercerNumero());
        
        predioDAO.update(predio);
    }

    @Override
    public void delete(Predio predio) throws Exception {
        validate(predio);
        Predio oldPredio = predioDAO.findById(predio.getIdPredio());
        if (oldPredio==null) {
            throw new Exception("ya existe un encuestador con la mismo cedula");
        }
        
        
        predioDAO.delete(predio);
    }

    @Override
    public Predio findById(Integer id_predio) throws Exception {
        if (id_predio ==0) {
           throw new Exception("El numero del formulario es obligatorio");
        }
        return predioDAO.findById(id_predio);
    }

    @Override
    public List<Predio> findAll() throws Exception {
        return predioDAO.findAll();
    }
}


