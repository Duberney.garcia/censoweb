/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.UsoComercial;
import co.edu.sena.censoweb.persistence.IUsoComercialDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * fecha:11/07/2022
 * @author Aprendiz
 * Objetivo: Implementar logica de negocio de uso comercial
 */
@Stateless
public class UsoComercialBean implements UsoComercialBeanLocal {
    
    
    @EJB
    private IUsoComercialDAO usoComercialDAO;
    
    public void validate(UsoComercial usoComercial) throws Exception{
        if(usoComercial ==null){
            throw new Exception("El uso comercial es nulo");
        }
        if (usoComercial.getIdUso()==0){
            throw new Exception("El id es obligatorio");
        }
        if(usoComercial.getDescripcion().isEmpty()){
          throw new Exception("La descripcion es obligatoria");
        
        }
    }
    
    @Override
    public void insert(UsoComercial usoComercial) throws Exception {
        validate(usoComercial);
        UsoComercial oldUsoComercial = usoComercialDAO.findById(usoComercial.getIdUso());
        if(oldUsoComercial != null){
            throw new Exception("Ya existe un uso comercial con el mismo ID");
        }
        usoComercialDAO.insert(usoComercial);
    }

    @Override
    public void update(UsoComercial usoComercial) throws Exception {
      validate(usoComercial);
        UsoComercial oldUsoComercial = usoComercialDAO.findById(usoComercial.getIdUso());
        if(oldUsoComercial == null){
            throw new Exception("NO existe el uso comercial");
        }
        //merge
        oldUsoComercial.setDescripcion(usoComercial.getDescripcion());
        usoComercialDAO.update(usoComercial);  
    }

    @Override
    public void delete(UsoComercial usoComercial) throws Exception {
        validate(usoComercial);
        UsoComercial oldUsoComercial = usoComercialDAO.findById(usoComercial.getIdUso());
        if(oldUsoComercial == null){
            throw new Exception("NO existe el uso comercial");
        }
        
        usoComercialDAO.delete(usoComercial);  
    }

    @Override
    public UsoComercial findById(Integer idUso) throws Exception {
        if(idUso ==0){
            throw new Exception("El id es obligatorio");
        }
        return usoComercialDAO.findById(idUso);
    }

    @Override
    public List<UsoComercial> findAll() throws Exception {
        return usoComercialDAO.findAll();
    }


    
}
