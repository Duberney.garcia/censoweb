/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.UsoComercial;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * fecha:11/07/2022
 * @author Aprendiz
 * Objetivo: Clase que implementa el DAO para uso comercial
 */
@Stateless
public class UsoComercialDAO implements IUsoComercialDAO{
    
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(UsoComercial usoComercial) throws Exception {
        try {
           entityManager.persist(usoComercial);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(UsoComercial usoComercial) throws Exception {
        try {
           entityManager.merge(usoComercial);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(UsoComercial usoComercial) throws Exception {
         try {
           entityManager.remove(usoComercial);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public UsoComercial findById(Integer idUso) throws Exception {
        
        try {
           return entityManager.find(UsoComercial.class, idUso);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<UsoComercial> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("UsoComercial.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }        
}
