/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Suscriptor;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import co.edu.sena.censoweb.persistence.ISuscriptorDAO;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class SuscriptorBean implements SuscriptorBeanLocal{
    
    @EJB
    private ISuscriptorDAO suscriptorDAO ;
    
     public void validate(Suscriptor suscriptor)throws Exception{
        if (suscriptor ==null) {
            throw new Exception("El suscriptor es nulo");
        }
        if (suscriptor.getDocumento()==0) {
            throw new Exception("El documento es obligatorio");
        }
         if (suscriptor.getTipoDocumento().isEmpty()) {
            throw new Exception("La tipo de documento es obligatoria");
        }
          if (suscriptor.getPrimerNombre().isEmpty()) {
            throw new Exception("El nombre es obligatoria");
        }
           if (suscriptor.getPrimerApellido().isEmpty()) {
            throw new Exception("El apellido es obligatoria");
        }
    }

    @Override
    public void insert(Suscriptor suscriptor) throws Exception {
       validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findById(suscriptor.getDocumento());
        if (oldSuscriptor!=null) {
            throw new Exception("ya existe un servicio con la mismo cedula");
        }
        
        suscriptorDAO.insert(suscriptor);
    }

    @Override
    public void update(Suscriptor suscriptor) throws Exception {
          validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findById(suscriptor.getDocumento());
        if (oldSuscriptor==null) {
            throw new Exception("ya existe un servicio con la mismo cedula");
        }
        //marge 
        oldSuscriptor.setPrimerApellido(suscriptor.getPrimerApellido());
        oldSuscriptor.setPrimerNombre(suscriptor.getPrimerNombre());
        oldSuscriptor.setSegundoApellido(suscriptor.getSegundoApellido());
        oldSuscriptor.setSegundoNombre(suscriptor.getSegundoNombre());
        oldSuscriptor.setTelefono(suscriptor.getTelefono());
        oldSuscriptor.setTipoDocumento(suscriptor.getTipoDocumento());
        oldSuscriptor.setEmail(suscriptor.getEmail());
        suscriptorDAO.update(suscriptor);
    }
    @Override
    public void delete(Suscriptor suscriptor) throws Exception {
        validate(suscriptor);
        Suscriptor oldSuscriptor = suscriptorDAO.findById(suscriptor.getDocumento());
        if (oldSuscriptor==null) {
            throw new Exception("ya existe un servicio con la mismo cedula");
        }
        
        suscriptorDAO.delete(suscriptor);
    }
    

    @Override
    public Suscriptor findById(long documento) throws Exception {
        if (documento ==0) {
           throw new Exception("El id es obligatorio");
        }
        return suscriptorDAO.findById(documento);
    }

    @Override
    public List<Suscriptor> findAll() throws Exception {
       return suscriptorDAO.findAll();
    }

    
}
