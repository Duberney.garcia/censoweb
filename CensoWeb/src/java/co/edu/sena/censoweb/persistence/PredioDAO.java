/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Predio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class PredioDAO implements IPredioDAO{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Predio predio) throws Exception {
        try {
           entityManager.persist(predio);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Predio predio) throws Exception {
        try {
           entityManager.merge(predio);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Predio predio) throws Exception {
         try {
           entityManager.remove(predio);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public Predio findById(Integer id_predio ) throws Exception {
        
        try {
           return entityManager.find(Predio.class, id_predio);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<Predio> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Predio.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
