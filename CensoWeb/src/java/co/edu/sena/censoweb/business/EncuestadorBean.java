/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuestador;
import co.edu.sena.censoweb.persistence.EncuestadorDAO;
import co.edu.sena.censoweb.persistence.IEncuestadorDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class EncuestadorBean implements EncuestadorBeanLocal {
    
    @EJB
    private IEncuestadorDAO encuestadorDAO;
    public void validate(Encuestador encuestador) throws Exception{
        if(encuestador==null){
            throw new Exception("El encuestador esta nulo");
        } 
        if (encuestador.getCedula()==0){
            throw new Exception("La cedula obligatoria");
        }
        if (encuestador.getNombre()==null){
            throw new Exception("El nombre es obligatorio");
        }
        
    }
    @Override
    public void insert(Encuestador encuestador) throws Exception {
        validate(encuestador);
        Encuestador oldEncuestador = encuestadorDAO.findById(encuestador.getCedula());
        if(oldEncuestador != null){
            throw new Exception("Ya existe un encuestador con la misma cedula");
        }
        encuestadorDAO.insert(encuestador);
    }

    @Override
    public void update(Encuestador encuestador) throws Exception {
        validate(encuestador);
        Encuestador oldEncuestador = encuestadorDAO.findById(encuestador.getCedula());
        if(oldEncuestador == null){
            throw new Exception("No existe un encuestador con la misma cedula");
        }
        oldEncuestador.setNombre(encuestador.getNombre());
        oldEncuestador.setTelefono(encuestador.getTelefono());
                
        encuestadorDAO.update(encuestador);
    }

    @Override
    public void delete(Encuestador encuestador) throws Exception {
        validate(encuestador);
        Encuestador oldEncuestador = encuestadorDAO.findById(encuestador.getCedula());
        if(oldEncuestador == null){
            throw new Exception("No existe un encuestador con la misma cedula");
        }
        encuestadorDAO.delete(encuestador);
    }
    

    @Override
    public List<Encuestador> findAll() throws Exception {
        return encuestadorDAO.findAll();
    }                                           

    @Override
    public Encuestador findById(long cedula) throws Exception {
        if(cedula==0){
            throw new Exception("La cedula es obligatoria");
        }
        return encuestadorDAO.findById(cedula);
    }
}
