/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Encuestador;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class EncuestadorDAO implements IEncuestadorDAO{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Encuestador encuestador) throws Exception {
        try {
           entityManager.persist(encuestador);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Encuestador encuestador) throws Exception {
        try {
           entityManager.merge(encuestador);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public void delete(Encuestador encuestador) throws Exception {
         try {
           entityManager.remove(encuestador);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public Encuestador findById(Long cedula ) throws Exception {
        
        try {
           return entityManager.find(Encuestador.class, cedula);
        } catch (RuntimeException e) {
            throw e;
        }
        
    }

    @Override
    public List<Encuestador> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Encuestador.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
}
