/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Servicio;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IServicioDAO {
    public void insert(Servicio servicio) throws Exception;
    public void update(Servicio servicio) throws Exception;
    public void delete(Servicio servicio) throws Exception;
    public Servicio findById(Integer id_servicio) throws Exception;
    public List<Servicio> findAll() throws Exception;
}
